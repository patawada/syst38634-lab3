package time;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

//@RunWith(Parameterized.class)

public class TimeTest {

	@Test
	public void testGetMilliSeconds() {

		// Time timeTest = new Time();
		int result = Time.getMilliSeconds("12:05:05:05");
		assertTrue("Seconds not calculated", result == 5);

	}

	@Test(expected = NumberFormatException.class)
	public void testGetMilliSecondsException() {

		// Time timeTest = new Time();
		int result = Time.getMilliSeconds("12:05:05:0054");
		assertTrue("Seconds not calculated", result == 5);

	}

	@Test
	public void testGetMilliSecondsBoundaryIn() {

		// Time timeTest = new Time();
		int result = Time.getMilliSeconds("12:05:05:999");
		assertTrue("Seconds not calculated", result == 999);

	}

	@Test(expected = NumberFormatException.class)
	public void testGetMilliSecondsBoundaryOut() {

		// Time timeTest = new Time();
		int result = Time.getMilliSeconds("12:05:05:1000");
		assertTrue("Seconds not calculated", result == 0);

	}

	/*
	 * public String timeToTest;
	 * 
	 * public TimeTest( String dataTime ) {
	 * 
	 * timeToTest = dataTime;
	 * 
	 * }
	 * 
	 * @Parameterized.Parameters public static Collection<Object [ ]> loadData( ) {
	 * 
	 * Object [ ][ ] data = { { "12:05:05" } }; return Arrays.asList( data );
	 * 
	 * 
	 * }
	 * 
	 * @Test public void getTotalSeconds() {
	 * 
	 * int seconds = Time.getTotalSeconds( this.timeToTest ); System.out.println(
	 * this.timeToTest ); assertTrue( "The seconds were not calculated propertly " ,
	 * seconds == 43505 );
	 * 
	 * }
	 */
}

/*
 * @Test public void testMain() { fail("Not yet implemented"); }
 * 
 * @Test public void getTotalSeconds() { Time timeTest = new Time(); int result
 * = timeTest.getTotalSeconds("123:123:123");
 * assertTrue("Seconds not calculated", result == 43505); }
 * 
 * /*@Test public void testGetSeconds() { fail("Not yet implemented"); }
 * 
 * @Test public void testGetTotalMinutes() { fail("Not yet implemented"); }
 * 
 * @Test public void testGetTotalHours() { fail("Not yet implemented"); }
 * 
 * @Test public void testObject() { fail("Not yet implemented"); }
 * 
 * @Test public void testGetClass() { fail("Not yet implemented"); }
 * 
 * @Test public void testHashCode() { fail("Not yet implemented"); }
 * 
 * @Test public void testEquals() { fail("Not yet implemented"); }
 * 
 * @Test public void testClone() { fail("Not yet implemented"); }
 * 
 * @Test public void testToString() { fail("Not yet implemented"); }
 * 
 * @Test public void testNotify() { fail("Not yet implemented"); }
 * 
 * @Test public void testNotifyAll() { fail("Not yet implemented"); }
 * 
 * @Test public void testWaitLong() { fail("Not yet implemented"); }
 * 
 * @Test public void testWaitLongInt() { fail("Not yet implemented"); }
 * 
 * @Test public void testWait() { fail("Not yet implemented"); }
 * 
 * @Test public void testFinalize() { fail("Not yet implemented"); }
 */
